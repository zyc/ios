//
//  ViewController.swift
//  App3
//
//  Created by Cleverson Sacramento on 13/07/17.
//  Copyright © 2017 Cleverson Sacramento. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource {
    
    private var dados: [(String, String)] = []
    
    @IBOutlet weak var tabelaView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        for i in 1...1000 {
            let tupla = ("Título \(i)", "Decrição do título \(i)")
            dados.append(tupla)
        }
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dados.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //let cell = UITableViewCell()
        let cell = tableView.dequeueReusableCell(withIdentifier: "celulaPadrao2", for: indexPath)

        cell.textLabel!.text  = dados[indexPath.row].0
        cell.detailTextLabel!.text = dados[indexPath.row].1

        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            dados.remove(at: indexPath.row)
            tabelaView.deleteRows(at: [indexPath], with: .automatic)
        }
    }
    
    @IBAction func swipe(_ sender: UISwipeGestureRecognizer) {
        print("swipe")
    }

    @IBAction func tap(_ sender: UITapGestureRecognizer) {
        print("tap")
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "DetalheVC" {
            let cell = tabelaView.cellForRow(at: tabelaView.indexPathForSelectedRow!)!
            
            let titleLabel = segue.destination.view.subviews[0] as! UILabel
            titleLabel.text = cell.textLabel!.text!
            
            let descLabel = segue.destination.view.subviews[1] as! UILabel
            descLabel.text = cell.detailTextLabel!.text!

            print("\(cell.textLabel!.text!) / \(cell.detailTextLabel!.text!)")
        }
    }

    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
}
