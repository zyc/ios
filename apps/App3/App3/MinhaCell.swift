//
//  MinhaCell.swift
//  App3
//
//  Created by Cleverson Sacramento on 13/07/17.
//  Copyright © 2017 Cleverson Sacramento. All rights reserved.
//

import UIKit

class MinhaCell: UITableViewCell {

    @IBOutlet weak var tituloLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
