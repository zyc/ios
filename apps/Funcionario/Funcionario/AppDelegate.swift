import UIKit
import CoreData
import AlecrimCoreData

func persistentContainer() -> PersistentContainer {
    return AppDelegate.instance().persistentContainer
}

func transactional(_ context: NSManagedObjectContext, _ code: @escaping () throws -> Void) rethrows -> Bool {
    let result : Bool
    
    do {
        try code()
        result = true
        
    } catch {
        context.rollback()
        result = false
        
        debugPrint("Rollback executado: \(error.localizedDescription)")
        
        throw error
    }
    
    AppDelegate.instance().save(context: context)
    return result
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    static func instance() -> AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        let context = persistentContainer.viewContext
        
        _ = transactional(context) {
            FuncionarioManager.cargaInicial(force: false, context: context)
        }
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    lazy var persistentContainer: PersistentContainer = {
        let name = "DB"
        let container = PersistentContainer(name: name, automaticallyLoadPersistentStores: false)
        self.loadPersistentStores(container)
        
        return container
    }()
    
    private func loadPersistentStores(_ container: PersistentContainer, attempt: Int = 0){
        container.loadPersistentStores{ storeDescription, error in
            
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        }
    }
    
    func save(context: NSManagedObjectContext) {
        if context.hasChanges {
            do {
                try context.save()
                debugPrint("Transação salva")
                
            } catch {
                let nserror = error as NSError
                debugPrint("Falha ao tentar finalizar a transação o contexto: \(nserror), \(nserror.userInfo)")
            }
        } else {
            debugPrint("Transação não-salva pois não houve mudanças")
        }
    }
}

