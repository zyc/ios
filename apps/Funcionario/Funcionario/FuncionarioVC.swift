import UIKit

class FuncionarioVC: UIViewController {
    
    var funcionario: Funcionario?
    
    @IBOutlet weak var matriculaView: UILabel!
    
    @IBOutlet weak var nomeView: UITextField!
    
    @IBOutlet weak var telefoneView: UITextField!
    
    override func viewDidLoad() {
        matriculaView.text = funcionario?.matricula
        nomeView.text      = funcionario?.nome
        telefoneView.text  = funcionario?.telefone
        funcionario        = nil
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        let context = persistentContainer().viewContext
        
        _ = transactional(context) {
            let matricula = self.matriculaView.text!
            let funcionario = FuncionarioManager.obter(matricula: matricula, context: context)

            funcionario?.nome      = self.nomeView.text
            funcionario?.telefone  = self.telefoneView.text
        }
    }
}
