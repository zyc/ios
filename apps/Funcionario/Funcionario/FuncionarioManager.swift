import Foundation
import CoreData

class FuncionarioManager {
    
    private init() {
    }
    
    static func cargaInicial(force: Bool = false, context: NSManagedObjectContext) {
        if force {
            context.funcionario.deleteAll()
        }
        
        if context.funcionario.count() == 0 {

            let funcionarios = [
                ("21082758", "Cleverson Sacramento", "71 99925-8283"),
                ("01098608", "Rodrigo Santiago",     "61 97126-8712"),
                ("21048177", "Rodrigo Armenio",      "61 984328555"),
                ("21082588", "Marcos Freire",        "61 98114-6481"),
                ("21079862", "Erick Guimarães",      "61 99611-7722"),
                ("21080406", "Hildequison Lima",     "98169-5930"),
                ("21085498", "Lenir Brito",          "99397-0940"),
                ("21048096", "Andrei Sibin",         "61 99988-0025"),
                ("21048045", "Erique Costa",         "61 99367-0849"),
                ("01093460", "Anderson Barbosa",     "61 2021-9253")
            ]
            
            funcionarios.forEach { tupla in
                let funcionario = context.funcionario.create()
                
                funcionario.matricula = tupla.0
                funcionario.nome      = tupla.1
                funcionario.telefone  = tupla.2
            }
        }
    }
    
    static func obterTotos(context: NSManagedObjectContext) -> [Funcionario] {
        return context.funcionario.sort(usingAttributeName: "nome").execute()
    }
    
    static func obter(matricula: String, context: NSManagedObjectContext) -> Funcionario? {
        return context.funcionario.first(where: { $0.matricula == matricula })
    }
    
    static func deletar(matricula: String, context: NSManagedObjectContext) {
        if let funcionario = obter(matricula: matricula, context: context) {
            context.funcionario.delete(funcionario)
        }
    }
}
