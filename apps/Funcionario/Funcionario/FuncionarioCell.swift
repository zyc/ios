//
//  FuncionarioCell.swift
//  Funcionario
//
//  Created by Cleverson Sacramento on 20/09/17.
//  Copyright © 2017 Cleverson Sacramento. All rights reserved.
//

import UIKit

class FuncionarioCell: UITableViewCell {

    @IBOutlet weak var matriculaView: UILabel!
    
    @IBOutlet weak var nomeView: UILabel!
    
    @IBOutlet weak var telefoneView: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
