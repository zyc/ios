import UIKit

class FuncionariosVC: UITableViewController {
    
    // MARK: - Propriedades
    
    private var funcionarios = [Funcionario]()
    
    private var linhaSelecionada: Int?
    
    // MARK: - Métodos privados
    
    static func carregaFuncionarios() -> [Funcionario] {
        let context = persistentContainer().viewContext
        return FuncionarioManager.obterTotos(context: context)
    }
    
    // MARK: - View Controller
    
    override func viewDidLoad() {
        funcionarios = FuncionariosVC.carregaFuncionarios()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if let linhaSelecionada = self.linhaSelecionada {
            tableView.reloadRows(at: [IndexPath(row: linhaSelecionada, section: 0)], with: .fade)
            self.linhaSelecionada = nil
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "edicaoSegue" {
            let funcionarioVC = segue.destination as! FuncionarioVC
            linhaSelecionada = tableView.indexPathForSelectedRow!.row
            funcionarioVC.funcionario = funcionarios[linhaSelecionada!]
        }
    }
    
    // MARK: - Table View Controller
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return funcionarios.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "funcionarioCell", for: indexPath) as! FuncionarioCell
        let funcionario = funcionarios[indexPath.row]
        
        cell.matriculaView.text = funcionario.matricula
        cell.nomeView.text      = funcionario.nome
        cell.telefoneView.text  = funcionario.telefone
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            
            let context = persistentContainer().viewContext
            _ = transactional(context) {
                let matricula = self.funcionarios[indexPath.row].matricula!
                FuncionarioManager.deletar(matricula: matricula, context: context)
                
                self.funcionarios.remove(at: indexPath.row)
                self.tableView.deleteRows(at: [indexPath], with: .fade)
            }
        }
    }
}

