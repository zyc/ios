import UIKit


class MyView: UIView {
    
    @IBOutlet weak var onoff: UISwitch!
    
    @IBOutlet weak var speed: UISlider!
    
    @IBOutlet weak var square: UIView!
    
    @IBAction func speedChanged() {
        if speed.value == speed.maximumValue {
            onoff.isEnabled = false
        } else if !onoff.isEnabled {
            onoff.isEnabled = true
        }
    }
    
    @IBAction func onoffChanged() {
        speed.isEnabled = onoff.isOn
    }
        
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        //print(touches.first?.location(in: self))
        
        let duration = Double(speed.maximumValue - speed.value + speed.minimumValue)
        //print(duration)
        
        if onoff.isEnabled && onoff.isOn {
            UIView.animate(withDuration: duration) {
                self.square.center = (touches.first?.location(in: self))!
            }
        } else {
            self.square.center = (touches.first?.location(in: self))!
        }
    }
}
