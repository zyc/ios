//
//  ViewController.swift
//  App1
//
//  Created by Cleverson Sacramento on 10/07/17.
//  Copyright © 2017 Cleverson Sacramento. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var onoffExp: UISwitch!
    
    @IBOutlet weak var speedExp: UISlider!
    
    @IBOutlet weak var square: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        onoffExp.addTarget(self, action: #selector(onoffExpChanged), for: .valueChanged)
        
        speedExp.addObserver(self, forKeyPath: "value", options: .new, context: nil)
        
        square.layer.cornerRadius = 50
        
        //print(onoffExp.isOn)
    }
    
    func onoffExpChanged() {
        let myView = view as! MyView
        myView.onoff.isOn = onoffExp.isOn
        myView.onoffChanged()
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        if keyPath == "value" {
            let myView = view as! MyView
            myView.speed.value = speedExp.value
            myView.speedChanged()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

