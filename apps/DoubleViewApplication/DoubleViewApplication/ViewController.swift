//
//  ViewController.swift
//  DoubleViewApplication
//
//  Created by Cleverson Sacramento on 20/09/17.
//  Copyright © 2017 Cleverson Sacramento. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var botaoVermelhoView: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        botaoVermelhoView.layer.cornerRadius = 10.0
        
        
        //title = "xxx"
        
        //navigationController?.title = "Primeiro VC"
        
        //navigationController?.navigationBar.title
        
        
        print("VC1: viewDidLoad")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        print("VC1: viewWillAppear")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print("VC1: viewDidAppear")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        print("VC1: viewWillDisappear")
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        print("VC1: viewDidDisappear")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

