//: Playground - noun: a place where people can play

import UIKit

func primos(ar: [Int]) -> [Int]{
    var arPrimos = [Int]()
    for numero in ar{
        if is_prime(numero: numero){
            arPrimos.append(numero)
        }
        
    }
    return arPrimos
}

func is_prime(numero: Int) -> Bool{
    if numero <= 1{
        return false
    }
    if numero <= 2{
        return true
    }
    
    var i = 2
    while i * i <= numero{
        if numero % i == 0{
            return false
        }
        i = i + 1
    }
    return true
    
}

var arTeste = [1,2,5,6,13,8]
primos(ar: arTeste)