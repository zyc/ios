import UIKit
import Alamofire
import AlamofireSwiftyJSON

class FotosVC: UITableViewController {
    
    private typealias PhotoTupla = (titulo: String, url: String)
    
    private var photos = [PhotoTupla]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //carregaDadosRemotos()
    }
    
    @IBAction func refresh() {
        carregaDadosRemotos {
            self.refreshControl?.endRefreshing()
            self.tableView.reloadData()
        }
    }
    
    private func carregaDadosRemotos(completou: @escaping () -> Void) {
        let url = "https://jsonplaceholder.typicode.com/photos"
        Alamofire.request(url).responseSwiftyJSON { response in
            
            response.result.value?.array?.enumerated().forEach { i, photoJSON in
                let photo: PhotoTupla = (titulo: photoJSON["title"].stringValue, url: photoJSON["url"].stringValue)
                self.photos.append(photo)
            }
            
            completou()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return photos.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //let cell = tableView.dequeueReusableCell(withIdentifier: "photoCell", for: indexPath)
        let cell = UITableViewCell()
        cell.imageView?.image = UIImage(named: "ImagePlaceholder")
        
        let photo = photos[indexPath.row]
        cell.textLabel?.text = photo.titulo
        
        // let activity = UIActivityIndicatorView()
        // activity.startAnimating()
        // cell.imageView?.addSubview(activity)
        // activity.center = cell.imageView!.center // Errado! CGPoint(x: myBlueSubview.bounds.size.width/2, y: myBlueSubview.bounds.size.height/2)
        
        Alamofire.request(photo.url).responseData { response in
            if let value = response.result.value {
                cell.imageView?.image = UIImage(data: value)
                // activity.stopAnimating()
                // activity.removeFromSuperview()
            }
        }
        
        return cell
    }
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
