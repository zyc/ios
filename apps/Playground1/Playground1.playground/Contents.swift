//: Playground - noun: a place where people can play

import Foundation


// Exemplo: function _

func imprimeDados(_ texto: String) {
    print("texto informado \(texto) com tamanho \(texto.characters.count)")
}

imprimeDados("oi5")


// Exemplo: function a, b

func soma(a: Int, b: Int) -> Int {
    return a+b
}

print("valor da soma: " + String(soma(a: 2, b: 4)))


// Exemplo: function a a, b b

func soma(primeiroNumero a: Int, comOSegundoNumero b: Int) -> Int {
    return a+b
}

soma(primeiroNumero: 2, comOSegundoNumero: 5)


// Exemplo: function _, _

func soma(_ a: Int, _ b: Int) -> Int {
    return a+b
}

soma(10, 20)


// Exemplo: array

let a1: [String] = ["nome1", "nome2"]

print(a1[0])

var a2: [String] = []

a2.append("nome1")
a2.append("nome2")
a2.append("nome3")
a2.remove(at: 1)
a2.insert("pessoa2", at: 1)
a2.reverse()
a2.sort()


// Exemplo: dictionary

var map: [String: Int] = [:]
map["k1"] = 10
map["k2"] = 20
map.updateValue(30, forKey: "k3")
map


// Exemplo: tuple

let tuple: (String, Int, Bool) = ("cachaça", 1, true)
tuple.0
tuple.1
tuple.2

let tuple2 = ("cachaça", 1, true)
tuple2.0
tuple2.1
tuple2.2
tuple2

let tuple3 = (nm: "cachaça", id: 1, av:true)
tuple3.0
tuple3.1
tuple3.2
tuple3.nm
tuple3.id
tuple3.av
tuple3

func retornaTupla() -> (Int, Bool) {
    var t: (Int, Bool)
    t.0 = 100
    t.1 = false
    
    return t
}

retornaTupla()

let tupleNomeada: (n: String, id: Int, disp: Bool)

tupleNomeada.n = "Vinho"
tupleNomeada.id = 44
tupleNomeada.disp = false

// Exemplo: for

let arrayFor = ["Stefani", "Josefa", "Creide"]

let dictFor = ["nome": "Stefani", "sobrenome": "Silva"]

let arrayOfTupleFor = [("Stefani", "Silva", 20), ("Josefa", "Rocha", 33), ("Creide", "Lima", 42)]

for v in arrayFor {
    print(v)
}

for (i, v) in arrayFor.enumerated() {
    print("índice \(i) e valor \(v)")
}

for (k, v) in dictFor {
    print ("\(k): \(v)")
}

for (n, sn, i) in arrayOfTupleFor {
    print ("\(n) \(sn) tem \(i) anos")
}

arrayFor.forEach { n in // closure: cenas dos próximos capítulos...
    print(n)
}


// Exemplo: optional chaining + forced unwrapping

var x: String?
print(x)
print(x ?? "vazio")
x = "Agora sim"
print(x) // Optional
print(x ?? "vazio")
print(x!)

func obterMatricula(cpf: String) -> String? {
    let funcionarios = [
        "111": "Stefani",
        "123": "Josefa",
        "678": "Creide"
    ]
    
    return funcionarios[cpf]
}

print(obterMatricula(cpf: "123")!)


// Exemplo: class

class FuncionarioVersao1 {
    
    var nome: String?
    
    var sobrenome: String?
    
    var idade: Int?
}

var func1 = FuncionarioVersao1()
func1.nome = "Stefani"
func1.sobrenome = "Silva"
func1.idade = 20

class FuncionarioVersao2: CustomStringConvertible {

    let nome: String

    let sobrenome: String

    let idade: Int
    
    var description: String {
        return "\(nome) \(sobrenome), \(idade) anos"
    }

    init(_ nome: String, _ sobrenome: String, _ idade: Int) {
        self.nome = nome
        self.sobrenome = sobrenome
        self.idade = idade
   }
}

var func2 = FuncionarioVersao2("Fulano", "de Tal", 50)
print("\(func2)")
