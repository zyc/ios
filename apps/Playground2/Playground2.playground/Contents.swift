//: Playground - noun: a place where people can play

import Foundation
import NetworkExtension
import CFNetwork


class Main: DetecotorDeMudancaDelegate {
    
    func ocorreuUmaMudanca() {
        print("delegate: \(Date())")
    }
}


let m = Main()

//let url = Bundle.main.url(forResource: "File", withExtension: nil)
let url = URL(string: "https://gitlab.com/zyc/ios/raw/master/README.md")!

let d = DetecotorDeMudanca(url)
d.delegate = m

d.iniciar {
    print("closure: \(Date())")
}

RunLoop.main.run()

//let url = URL(string: "http://dontpad.com/ios")
//let atual = try Data(contentsOf: url!).hashValue
