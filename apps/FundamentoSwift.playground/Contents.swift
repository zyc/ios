//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"


func soma1(a: Int, b: Int) -> Int {
    return a + b
}

soma1(a: 1, b: 2)

func soma2(_ a: Int, _ b: Int) -> Int {
    return a + b
}

soma2(1, 2)

func soma3(valorA a:Int, comValorB b:Int) -> Int {
    return a + b
}

soma3(valorA: 2, comValorB: 3)

var var1: Int
let const1: Int

var var2 = 1
var var3: Int16 = 1

func soma4(a: Int, b: Int, c:Int? = 0) -> Int {
    return a + b + (c ?? 0)
}

soma4(a: 4, b: 4)
soma4(a: 1, b: 2, c: 3)
soma4(a: 1, b: 2, c: nil)

let string1 = "teste1"
print(string1)

var string2: String? = "teste2"
string2 = nil
print(string2 ?? ".")

let c1 = 1
let c2 = "String"
let c3 = 1.1

//print(c1 + ", " + c2 + ", " + c3)
print("aqui é c1: \"\(c1)\", c2: \(c2), o último: \(c3)")

var array1 = ["1", "2", "3"]
var array2 = [1, 2, 3]
var array3: [Any] = ["1", 2, 3]

var array4: [String] = []
var array5 = [String]()
var string3 = String()

array5.append("1")
array5[0]
array5.remove(at: 0)

let array6: [String]
array6 = []
//array6.append("4")

for elem in array1 {
    print(elem)
}

array1.forEach { elem in
    print(elem)
}

var dict1: [String: Int]
var dict2 = [String: Int]()

dict2.updateValue(1, forKey: "chave1")
dict2["chave2"] = 2
dict2["chave1"]
dict2


func operacoes1(a: Int, b: Int) -> (Int, Int, String) {
    return (a + b, a * b, "\(a)\(b)")
}

let tupla1 = operacoes1(a: 5, b: 6)
tupla1.0
tupla1.1
tupla1.2

func operacoes2(a: Int, b: Int) -> (soma: Int, mult: Int, concat: String) {
    return (a + b, a * b, "\(a)\(b)")
}

let tupla2 = operacoes2(a: 2, b: 3)
tupla2.0
tupla2.soma


class Classe1 {
    
    typealias FuncaoRecStr = (String) -> Void
    
    let attr1: String
    
    init(_ attr1: String) {
        self.attr1 = attr1
    }
    
    func met1(code: () -> Void) {
        code()
    }
    
    func met2(code: FuncaoRecStr) {
        code(attr1)
    }
}

func func1() {
    print("minha função")
}

func func2(_ string: String) {
    print(string)
}

let cl1 = Classe1("teste")
cl1.met1 {
    print("meu closure")
}

cl1.met1(code: func1)
cl1.met2(code: func2)

cl1.met2(code: { str in
    print(str)
})

cl1.met2 { str in
    print(str)
}
