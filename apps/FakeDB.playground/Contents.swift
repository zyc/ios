//: Playground - noun: a place where people can play

import Foundation

//"".con

func prime(_ number: Int) -> Bool {
    return (1...number).filter({ number % $0 == 0 }).count <= 2
}

prime(11)

func primes(_ numbers: [Int]) -> [Int] {
    return numbers.filter { prime($0) }
}

primes([1, 3, 6, 7, 13, 44, 131, 2, 200])





var str = "Hello, playground"


class Funcionario: CustomStringConvertible {
    
    var nome: String
    
    var matricula: String
    
    init(nome: String, matricula: String) {
        self.nome = nome
        self.matricula = matricula
    }
    
    var description: String {
        get {
            return "\(matricula): \(nome)"
        }
    }
}

class DB {
    
    typealias Filter<T> = (T) -> Bool
    
    typealias X<T> = T.Type
    
    var tables =  [String: [Any]]()
    
    func insert(_ object: Any, table: String) {
        var values = tables[table]
        
        if values == nil {
            values = [Any]()
        }
        
        values!.append(object)
        tables[table] = values!
    }
    
    func getAll(table: String) -> [Any]? {
        return tables[table]
    }
    
    func get<T>(table: String, type: T.Type, filter: Filter<T>) -> [T]? {
        var result = [T]()
        
        getAll(table: table)?.forEach { object in
            if let object = object as? T {
                if filter(object) {
                    result.append(object)
                }
            }
        }
        
        return result.isEmpty ? nil : result
    }
    
    func delete<T>(table: String, type: T.Type, filter: Filter<T>) {
        var result = [Any]()

        getAll(table: table)?.forEach { object in
            if let object = object as? T {
                if !filter(object) {
                    result.append(object)
                }
            }
        }

        tables[table] = result
    }
}


let db = DB()

db.insert("teste1", table: "opa")
db.insert("teste2", table: "opa")
db.insert("testando", table: "opa")

print(db.getAll(table: "opa") ?? ())

db.insert(Funcionario(nome: "Cleverson", matricula: "12345"), table: "func")
db.insert(Funcionario(nome: "Teste", matricula: "123"), table: "func")

print(db.get(table: "func", type: Funcionario.self, filter: { $0.nome == "Cleverson" }) ?? [])
print(db.getAll(table: "func") ?? [])
db.delete(table: "func", type: Funcionario.self, filter: { $0.nome.contains("Clev") })
print(db.getAll(table: "func") ?? [])
