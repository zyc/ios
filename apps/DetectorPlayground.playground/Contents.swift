import Foundation

var str = "Hello, playground"


protocol DetectorDelegate {
    
    func ocorreuMudanca(_ hash: Int)
    
}

class Detector {
    
    private let url: String
    
    private var ultimoHash: Int?
    
    var delegate: DetectorDelegate?
    
    init(_ url: String) {
        self.url = url
    }

    func iniciar(_ codigo: @escaping ((Int) -> Void) = { _ in } ) {
        
        Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { _ in
            
            if let url = URL(string: self.url) {
                do{
                    let data = try Data(contentsOf: url)
                    let newHash = data.hashValue

                    if let ultimoHash = self.ultimoHash {
                        if ultimoHash != newHash {
                            self.ultimoHash = newHash
                            
                            codigo(newHash)
                            self.delegate?.ocorreuMudanca(newHash)
                            
                        }
                    } else {
                        self.ultimoHash = newHash
                    }
                    
                } catch {
                    debugPrint(error.localizedDescription)
                }
            }
        }
    }
}

class Ouvidor: DetectorDelegate {
    
    func ocorreuMudanca(_ hash: Int) {
        print("No delegado \(hash)")
    }
}


let detec = Detector("https://gitlab.com/zyc/ios/raw/master/README.md")
detec.delegate = Ouvidor()


//detec.ini
detec.iniciar { hash in
    print("No closure \(hash)")
}

//detec.iniciar()

RunLoop.main.run()
