//
//  ViewController.swift
//  Detector
//
//  Created by Cleverson Sacramento on 19/09/17.
//  Copyright © 2017 Cleverson Sacramento. All rights reserved.
//

import UIKit

class ViewController: UIViewController, DetectorDelegate {

    @IBOutlet weak var hashView: UILabel!
    
    @IBOutlet weak var hashClosureView: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let d = Detector("https://gitlab.com/zyc/ios/raw/master/README.md")
        d.delegate = self
        
        d.iniciar { self.hashClosureView.text = String($0) }
    }
    
    func ocorreuMudanca(_ hash: Int) {
        hashView.text = String(hash)
    }
}

