import Foundation
import NSDate_TimeAgo

extension Date {
    
    public func timeAgo() -> String {
        return (self as NSDate).timeAgo()
    }
}
