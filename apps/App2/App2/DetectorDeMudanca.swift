import Foundation
import NetworkExtension

public protocol DetecotorDeMudancaDelegate {
    
    func ocorreuUmaMudanca()
}

open class DetecotorDeMudanca {
    
    public var delegate: DetecotorDeMudancaDelegate? = nil
    
    private var bloco: (() -> ())?
    
    private var timer: Timer?
    
    private var ultimo: Int?
    
    private let url: URL
    
    public init(_ url: URL) {
        self.url = url
    }
    
    public func iniciar() {
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(procura), userInfo: nil, repeats: true)
        timer!.fire()
    }
    
    public func iniciar( _ bloco : @escaping () -> ()) {
        self.bloco = bloco
        iniciar()
    }
    
    public func parar() {
        timer?.invalidate()
    }
    
    @objc private func procura() throws {
        //let url = Bundle.main.url(forResource: "File", withExtension: nil)
        
        URLCache.shared = URLCache(memoryCapacity: 0, diskCapacity: 0, diskPath: nil)
        //let url = URL(string: "http://dontpad.com/serpro-ios")
        
        let atual = try Data(contentsOf: url).hashValue
        //print(try String(contentsOf: url))
        
        if let ultimoNaoNulo = ultimo {
            if (atual != ultimoNaoNulo) {
                
                bloco?()
                delegate?.ocorreuUmaMudanca()
                
                ultimo = atual
            }
        } else {
            print(url)
            ultimo = atual
        }
    }
}
