//
//  ViewController.swift
//  App2
//
//  Created by Cleverson Sacramento on 12/07/17.
//  Copyright © 2017 Cleverson Sacramento. All rights reserved.
//

import UIKit

class ViewController: UIViewController, DetecotorDeMudancaDelegate {
    
    @IBOutlet weak var horaMudanca: UIDatePicker!
    
    @IBOutlet weak var tempoQuePassou: UILabel!
    
    private var ultimaAtualizacao: Date?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let url = URL(string: "https://gitlab.com/zyc/ios/raw/master/README.md")!
        
        let d = DetecotorDeMudanca(url)
        d.delegate = self
        
        d.iniciar {
            // self.ultimaAtualizacao = Date()
        }
        
        Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { _ in
            self.atualizaTempoQuePassou()
        }
    }
    
    func ocorreuUmaMudanca() {
        ultimaAtualizacao = Date()
        horaMudanca.date = ultimaAtualizacao!
        atualizaTempoQuePassou()
    }
    
    func atualizaTempoQuePassou () {
        if let tempo = ultimaAtualizacao {
            tempoQuePassou!.text = tempo.timeAgo()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

